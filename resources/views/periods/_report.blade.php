<table width="98%" border="1">
    <tr>
        <th>TYPES</th>
        @foreach(array_keys(reset($master)) as $header)
            <th>{{ strtoupper($header) }}</th>
        @endforeach
    </tr>
    @foreach($master as $key=>$row)
        <tr>
            <td>{{ strtoupper($key) }}</td>
            @foreach($row as $cell)
                <td>{{ strtoupper($cell) }}</td>
            @endforeach
        </tr>
    @endforeach
</table>

<table width="40%" border="1" style="margin-top: 15px">
    <tr>
        <th>PERFORMING</th>
        <td>{{ $npaData['performing'] }}</td>
    </tr>
    <tr>
        <th>NON-PERFORMING</th>
        <td>{{ $npaData['non-performing'] }}</td>
    </tr>
    <tr>
        <th>NPA</th>
        <td>{{ $npaData['npa'] }}</td>
    </tr>
</table>
