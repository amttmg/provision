@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create New Provision</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('period.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="year">Enter Year</label>
                                <select name="year" id="year" class="@error('year') is-invalid @enderror form-control">
                                    <option value="2077">2077</option>
                                    <option value="2078">2078</option>
                                    <option value="2079">2079</option>
                                    <option value="2080">2080</option>
                                </select>
                                @error('year')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="quarter">Enter Quarter</label>
                                <select name="quarter" id=""
                                        class="@error('quarter') is-invalid @enderror form-control">
                                    <option value="1">1st Quarter</option>
                                    <option value="2">2st Quarter</option>
                                    <option value="3">3st Quarter</option>
                                    <option value="4">4st Quarter</option>
                                </select>
                                @error('quarter')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary">Submit</button>
                                <a href="{{ route('home') }}" class="btn btn-danger" type="button">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
