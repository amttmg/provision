@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <table>
                <tr>
                    <td><h1 class="text-center">Rastriya Banijya Bank Ltd</h1></td>
                </tr>
                <tr>
                    <td>
                        <h3 class="text-center">{{ auth()->user()->name }}</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4 class="text-center">{{ $period->name }}</h4>
                    </td>
                </tr>
            </table>
            @include('periods/_report', ['master'=>$master, 'npaData'=>$npaData])
        </div>
    </div>

@endsection
