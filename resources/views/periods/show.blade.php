@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if(!$period->loans->count())
                    <div class="card">
                        <div class="card-header">Upload Loan Os For {{ $period->name }}</div>
                        <div class="card-body">
                            <div class="row justify-content-center">
                                <form action="{{ route('upload', $period->id) }}" method="post"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="file" class="form-label">Please upload loan os file</label>
                                        <input type="file" name="file" accept=".xlsx"/>
                                        <button class="btn btn-sm btn-primary">Upload</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="">
                        <div class="">
                            <div class="row">
                                <div class="col-md-6 d-flex align-items-center">
                                    <h2>Loan os of <b>{{ $period->name }}</b></h2>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ route('reset', $period->id) }}" class="btn btn-primary"
                                       style="float: right">
                                        <b-icon icon="arrow-clockwise"></b-icon>
                                        Reset & Upload again</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <provisions :id="{{ $period->id }}"></provisions>
                @endif
            </div>

        </div>
    </div>
@endsection
