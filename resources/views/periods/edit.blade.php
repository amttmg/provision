@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Update New Provision</div>
                    <div class="card-body">
                        <form method="post" action="{{ route('period.update', $period->id) }}"
                              enctype="multipart/form-data">
                            @method('patch')
                            @csrf
                            <div class="form-group">
                                <label for="name">Enter Title Of Provision</label>
                                <input type="text" class="@error('name') is-invalid @enderror form-control" id="name"
                                       placeholder="जस्तै: २०७७ पहिलो त्रैमास"
                                       name="name"
                                       value="{{ old('name', $period->name) }}"
                                >
                                @error('name')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="year">Enter Year</label>
                                <input type="text" class="@error('year') is-invalid @enderror form-control" id="name"
                                       placeholder="Enter Year"
                                       name="year"
                                       value="{{ old('year', $period->year) }}"
                                >
                                @error('year')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="quarter">Enter Quarter</label>
                                <select name="quarter"
                                        class="@error('quarter') is-invalid @enderror form-control">
                                    <option {{ old('quarter', $period->quarter) == 1 ? 'selected' : '' }} value="1">
                                        1st Quarter
                                    </option>
                                    <option {{ old('quarter', $period->quarter) == 2 ? 'selected' : '' }} value="2">2st
                                        Quarter
                                    </option>
                                    <option {{ old('quarter', $period->quarter) == 3 ? 'selected' : '' }} value="3">3st
                                        Quarter
                                    </option>
                                    <option {{ old('quarter', $period->quarter) == 4 ? 'selected' : '' }} value="4">4st
                                        Quarter
                                    </option>
                                </select>
                                @error('quarter')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary">Submit</button>
                                <a href="{{ route('home') }}" class="btn btn-danger" type="button">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
