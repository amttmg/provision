@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if($periods->count())
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ route('period.create') }}" class="btn btn-sm btn-primary">New Provision</a>
                        </div>
                        <table class="table table-striped">
                            <tr>
                                <th>Name</th>
                                <th>Year</th>
                                <th>Quartar</th>
                                <th>Action</th>
                            </tr>

                            @forelse($periods as $period)
                                <tr>
                                    <td>{{ $period->name }}</td>
                                    <td>{{$period->year}}</td>
                                    <td>{{$period->quarter}}</td>
                                    <td>
                                        <a href="{{ route('period.show', $period->id) }}" class="btn btn-sm btn-primary">View</a>
                                        <a href="{{ route('period.edit', $period->id) }}"
                                           class="btn btn-sm btn-primary">Edit</a>
                                        <button
                                            onclick="event.preventDefault(); document.getElementById('form{{ $period->id }}').submit();"
                                            type="button" class="btn btn-danger btn-sm">Delete
                                        </button>
                                        <form id="form{{ $period->id }}" style="display: none"
                                              action="{{route('period.destroy',$period->id)}}"
                                              method="POST">
                                            @method('DELETE')
                                            @csrf
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4" class="text-center">No Record Found please add new</td>
                                </tr>
                            @endforelse
                        </table>

                    </div>
                @else
                    <div class="card">
                        <div class="card-body text-center">
                            <a href="{{ route('period.create') }}" class="btn btn-primary">
                                Make New Provision
                            </a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
