<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->id();
            $table->integer('period_id');
            $table->string('BranchCode');
            $table->string('ac_type_id');
            $table->string('MainCode');
            $table->string('FirmName');
            $table->string('Pro_Name');
            $table->string('ClientCode');
            $table->double('Limit');
            $table->double('Balance');
            $table->float('PenalInt');
            $table->float('IntOnInt');
            $table->float('NormalInt');
            $table->float('IntOs');
            $table->string('Class');
            $table->integer('loan_class_id');
            $table->string('Status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
