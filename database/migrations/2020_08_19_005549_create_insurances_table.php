<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurances', function (Blueprint $table) {
            $table->id();
            $table->string("name", 200);
            $table->integer('branch_code');
            $table->string("main_code")->nullable();
            $table->float("insurance_amount", 50);
            $table->float("premium_amount", 50);
            $table->string("policy_number", 100);
            $table->date("issue_date");
            $table->date("expire_date");
            $table->integer("company_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurances');
    }
}
