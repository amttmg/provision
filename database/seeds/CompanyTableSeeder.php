<?php

use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = [
            'NLG Insurance Company',
            'NECO Insurance Company',
            'Prime Insurance Company',
            'Shikhar Insurance',
        ];

        foreach ($companies as $company) {
            \App\Company::updateOrCreate(
                ['name' => $company],
                [
                    'branch_code' => 201,
                    'name'        => $company,
                ]
            );
        }
    }
}
