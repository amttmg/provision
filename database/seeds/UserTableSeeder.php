<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::updateOrCreate(
            ['username' => 'RBB201'],
            [
                'name'        => 'Branch Office Dhankuta',
                'branch_code' => '201',
                'username'    => 'RBB201',
                'email'       => 'dhankuta@rbb.com.np',
                'password'    => bcrypt('dhankuta@1234'),
            ]);
    }
}
