<?php

use App\LoanClass;
use Illuminate\Database\Seeder;

class LoanClassTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $classes = [
            [
                'id'        => 1,
                "name"      => "GOOD",
                "type"      => "PERFORMING",
                "provision" => 1,
                "status"    => true,
            ],
            [
                'id'        => 2,
                "name"      => "WATCH_LIST",
                "type"      => "PERFORMING",
                "provision" => 5,
                "status"    => true,
            ],
            [
                'id'        => 3,
                "name"      => "SUB",
                "type"      => "NOT PERFORMING",
                "provision" => 25,
                "status"    => true,
            ],
            [
                'id'        => 4,
                "name"      => "DOUBT",
                "type"      => "NOT PERFORMING",
                "provision" => 50,
                "status"    => true,
            ],
            [
                'id'        => 5,
                "name"      => "BAD",
                "type"      => "NOT PERFORMING",
                "provision" => 100,
                "status"    => true,
            ],

        ];
        foreach ($classes as $class) {
            LoanClass::updateOrCreate(
                ['id' => $class['id']],
                $class
            );
        }

    }
}
