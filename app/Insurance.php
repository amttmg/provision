<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
    protected $fillable = [
        'name',
        'branch_code',
        'main_code',
        'insurance_amount',
        'premium_amount',
        'policy_number',
        'issue_date',
        'expire_date',
        'company_id',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
