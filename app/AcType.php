<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcType extends Model
{
    protected $fillable = ['code', 'name'];

    public function loans()
    {
        return $this->hasMany(Loan::class);
    }
}
