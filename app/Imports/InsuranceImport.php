<?php

namespace App\Imports;

use App\Insurance;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Validator;


class InsuranceImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        $branchCode = auth()->user()->branch_code;
        Validator::make($rows->toArray(), [
            '*.name'             => 'required',
            '*.main_code'        => 'required',
            '*.insurance_amount' => 'required|numeric',
            '*.premium_amount'   => 'required|numeric',
            '*.policy_number'    => 'required',
            '*.issue_date'       => 'required|date',
            '*.expire_date'      => 'required|date',
            '*.company_id'       => 'required|exists:companies,id',
        ])->validate();
        foreach ($rows as $row) {
            $data = [
                'branch_code'      => $branchCode,
                'name'             => $row['name'],
                'main_code'        => $row['main_code'],
                'insurance_amount' => $row['insurance_amount'],
                'premium_amount'   => $row['premium_amount'],
                'policy_number'    => $row['policy_number'],
                'issue_date'       => Carbon::parse($row['issue_date']),
                'expire_date'      => Carbon::parse($row['expire_date']),
                'company_id'       => $row['company_id'],
            ];
            Insurance::updateOrCreate(
                ['main_code' => $row['main_code']],
                $data
            );
        }
    }
}
