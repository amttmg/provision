<?php

namespace App\Imports;

use App\AcType;
use App\Loan;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithStartRow;

class LoanImport implements ToModel, withHeadingRow, WithMultipleSheets, withStartRow
{
    private $periodId;

    /**
     * LoanImport constructor.
     * @param $periodId
     */
    public function __construct($periodId)
    {
        $this->periodId = $periodId;
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        if (!isset($row['actype']) || $row['maincode'] == '~~~') {
            return null;
        }
        try {
            return new Loan([
                'period_id'     => $this->periodId,
                'BranchCode'    => auth()->user()->branch_code,
                'ac_type_id'    => $this->getAcTypeId($row['actype']),
                'MainCode'      => $row['maincode'],
                'FirmName'      => $row['firmname'],
                'Pro_Name'      => $row['firmname'],
                'ClientCode'    => $row['clientcode'],
                'Limit'         => $row['limit'],
                'Balance'       => round($row['balance'], 2),
                'PenalInt'      => round($row['penalint'], 2),
                'IntOnInt'      => round($row['intonint'], 2),
                'NormalInt'     => round($row['normalint'], 2),
                'IntOs'         => round($row['intos'], 2),
                'Class'         => $row['class'],
                'Status'        => $row['status'],
                'loan_class_id' => $this->getLoanClassId($row['class']),
            ]);
        } catch (\Exception $exception) {
            dd($row, $exception);
        }
    }

    private function getAcTypeId($type)
    {
        $acType = AcType::where('code', $type)->first();
        if ($acType) {
            return $acType->id;
        } else {
            return AcType::create([
                'code' => $type,
                'name' => '-',
            ])->id;
        }
    }

    private function getLoanClassId($class)
    {
        $classes = [
            "GOOD"       => 1,
            "WATCH_LIST" => 2,
            "SUB"        => 3,
            "DOUBT"      => 4,
            "BAD"        => 5,
        ];

        return $classes[$class];
    }

    /**
     * @inheritDoc
     */
    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }

    /**
     * @inheritDoc
     */
    public function startRow(): int
    {
        return 2;
    }
}
