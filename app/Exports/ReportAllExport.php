<?php

namespace App\Exports;

use App\LoanClass;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ReportAllExport implements WithMultipleSheets
{
    private $periodId;

    /**
     * ReportExport constructor.
     * @param $periodId
     */
    public function __construct($periodId)
    {
        $this->periodId = $periodId;
    }

    public function sheets(): array
    {
        $sheets[]    = new ProvisionExport($this->periodId);
        $loanClasses = LoanClass::all();
        foreach ($loanClasses as $class) {
            $sheets[] = new LoanClassReportExport($this->periodId, $class);
        }

        return $sheets;
    }
}
