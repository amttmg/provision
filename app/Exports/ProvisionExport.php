<?php

namespace App\Exports;

use App\Services\ReportServices;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;

class ProvisionExport implements FromView, ShouldAutoSize, WithTitle
{
    private $periodId;

    /**
     * ReportExport constructor.
     * @param $periodId
     */
    public function __construct($periodId)
    {
        $this->periodId = $periodId;
    }

    public function view(): View
    {
        $master  = app(ReportServices::class)->getReportData($this->periodId);
        $npaData = app(ReportServices::class)->getNpa($this->periodId);

        return view('periods._report', compact('master', 'npaData'));
    }

    public function title(): string
    {
        return "PROVISION";
    }
}
