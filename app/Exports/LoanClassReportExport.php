<?php

namespace App\Exports;

use App\Loan;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class LoanClassReportExport implements FromArray, ShouldAutoSize, WithTitle, WithHeadings
{
    private $periodId;
    private $class;

    /**
     * ReportExport constructor.
     * @param $periodId
     * @param $class
     */
    public function __construct($periodId, $class)
    {
        $this->periodId = $periodId;
        $this->class    = $class;
    }

    public function title(): string
    {
        return $this->class->name;
    }

    public function array(): array
    {
        return Loan::select($this->headings())
            ->where('period_id', $this->periodId)
            ->where('loan_class_id', $this->class->id)
            ->get()
            ->toArray();
    }

    public function headings(): array
    {
        return [
            'BranchCode',
            'MainCode',
            'FirmName',
            'Pro_Name',
            'ClientCode',
            'Limit',
            'Balance',
            'PenalInt',
            'IntOnInt',
            'NormalInt',
            'NormalInt',

        ];
    }
}
