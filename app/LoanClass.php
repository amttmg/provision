<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanClass extends Model
{
    protected $fillable = ['name', 'status'];

    public function loans()
    {
        return $this->hasMany(Loan::class);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
