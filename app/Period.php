<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    protected $fillable = ['name', 'year', 'quarter'];

    public function loans()
    {
        return $this->hasMany(Loan::class);
    }
}
