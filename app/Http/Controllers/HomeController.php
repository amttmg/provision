<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    public function index()
    {
        $periods = auth()->user()->periods()->latest()->get();

        return view('home', compact('periods'));
    }
}
