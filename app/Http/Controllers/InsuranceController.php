<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\InsuranceRequest;
use App\Imports\InsuranceImport;
use App\Imports\LoanImport;
use App\Insurance;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class InsuranceController extends Controller
{
    public function index()
    {
        return view('insurance.index');
    }

    public function getCompanies()
    {
        $companies = Company::where('branch_code', auth()->user()->branch_code)->get();

        return response($companies);
    }

    public function getInsurances(Request $request)
    {
        $txtSearch  = $request->searchTxt;
        $expDay     = $request->expireDay;
        $insurances = Insurance::query()->with('company');
        if ($txtSearch) {
            $insurances = $insurances
                ->where('name', 'like', '%'.$txtSearch.'%')
                ->orWhere('main_code', 'like', '%'.$txtSearch.'%');
        }
        if (is_numeric($expDay)) {
            if ($expDay > 0) {
                $insurances = $insurances->whereBetween('expire_date',
                    [Carbon::now()->toDateString(), Carbon::now()->addDay($expDay)->toDateString()]);
            } elseif ($expDay <= 0) {
                $insurances = $insurances->whereDate('expire_date', '<', Carbon::now()->toDateString());
            }
        }
        $insurances = $insurances->get();

        $insurances->map(function ($item) {
            $item->dayToExpire = Carbon::now()->diffInDays($item->expire_date, false);
        });
        $order = $request->order;
        if ($order) {
            $insurances = $insurances->sortBy(function ($item) {
                return $item->dayToExpire;
            })->values();
        }


        return response($insurances);
    }

    public function update(InsuranceRequest $request, $id)
    {
        $ins = Insurance::find($id);

        return $ins->update($request->all());
    }

    public function store(InsuranceRequest $request)
    {
        return Insurance::updateOrCreate([
            "main_code" => $request->main_code,
        ],
            $request->all()
        );
    }

    public function batchImport(Request $request)
    {
        $res = app(\Maatwebsite\Excel\Excel::class)->import(new InsuranceImport(), $request->file('file'), null,
            \Maatwebsite\Excel\Excel::XLSX);

        return response(true);
    }

    public function delete($id)
    {
        $insurance = Insurance::where('id', $id)->delete();

        return response($insurance);
    }
}
