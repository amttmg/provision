<?php


namespace App\Http\Controllers\Api;


use App\AcType;
use App\Http\Controllers\ApiController;
use App\Loan;
use App\LoanClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoanApiController extends ApiController
{
    function getLoans(Request $request, $id)
    {
        $acType = $request->acType ?? null;

        return $this->getLoansResponse($id, $acType);
    }

    private function getLoansResponse($id, $acType = null)
    {
        $loanClasses = LoanClass::active()->orderBy('id')->get();
        $master      = [];
        foreach ($loanClasses as $class) {
            $query = AcType::with([
                'loans' => function ($query) use ($id, $class) {
                    $query->where('loan_class_id', $class->id)->where('period_id', $id);
                },
            ])->whereHas('loans', function ($query) use ($id, $class) {
                $query->where('loan_class_id', $class->id)->where('period_id', $id);
            });
            if ($acType) {
                $query->where('id', $acType);
            }
            $master[$class->name] = $query->get();
        }

        return response([
            'loans'       => $master,
            "loanClasses" => $this->getLoanClasses(),
            "acTypes"     => $this->getAcTypes(),
        ]);
    }

    public function getLoanClasses()
    {
        return LoanClass::active()->get();
    }

    public function getAcTypes()
    {
        return AcType::select(DB::raw("id as value, code as text"))->get();
    }

    public function updateLoanClass(Request $request)
    {
        return Loan::find($request->loanId)->update(['loan_class_id' => $request->classId]);
    }
}
