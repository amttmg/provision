<?php

namespace App\Http\Controllers;

use App\Exports\ReportAllExport;
use App\Imports\LoanImport;
use App\Loan;
use App\Period;
use App\Services\ReportServices;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;

class PeriodController extends Controller
{
    /**
     * @var ReportServices
     */
    private $reportServices;

    /**
     * PeriodController constructor.
     * @param ReportServices $reportServices
     */
    public function __construct(ReportServices $reportServices)
    {
        $this->reportServices = $reportServices;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('periods.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $suffix = ['', 'st', 'nd', 'rd', 'th'];
        $request->validate([
            'year'    => 'required|numeric',
            'quarter' => 'required|numeric',
        ]);

        $user   = auth()->user();
        $period = $user->periods()->create([
            'name'    => $request->quarter.$suffix[$request->quarter].' quarter of '.$request->year,
            'year'    => $request->year,
            'quarter' => $request->quarter,
        ]);

        return redirect()->route('period.show', $period->id);
    }

    public function show($id)
    {
        $period = Period::with([
            'loans',
            'loans.acType',
        ])->findOrFail($id);


        return view('periods.show', compact('period'));
    }

    public function report($periodId)
    {
        $period  = Period::findOrFail($periodId);
        $master  = $this->reportServices->getReportData($periodId);
        $npaData = $this->reportServices->getNpa($periodId);

        return view('periods.reports', compact('master', 'period', 'npaData'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $period = Period::find($id);

        return view('periods.edit', compact('period'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'    => 'required',
            'year'    => 'required|numeric',
            'quarter' => 'required|numeric',
        ]);

        $quarter = Period::find($id);
        $quarter->update($request->only('name', 'year', 'quarter'));

        return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Period::destroy($id);

        return redirect()->route('home');

    }

    public function upload(Request $request, $id)
    {
        Loan::where('period_id', $id)->delete();
        app(Excel::class)->import(new LoanImport($id), $request->file('file'), null, \Maatwebsite\Excel\Excel::XLSX);

        return redirect()->back();
    }

    public function reset($id)
    {
        Loan::where('period_id', $id)->delete();

        return redirect()->back();
    }

    public function export($periodId)
    {
        return app(Excel::class)->download(new ReportAllExport($periodId), 'report.xlsx');
    }
}
