<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InsuranceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'main_code'        => "required",
            'name'             => "required",
            'company_id'          => "required",
            'insurance_amount' => "required",
            'premium_amount'   => "required",
            'issue_date'       => "required",
            'expire_date'      => "required",
            'policy_number'    => "required",
        ];
    }
}
