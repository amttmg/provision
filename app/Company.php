<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['branch_code', 'name', 'status'];

    public function insurances()
    {
        return $this->hasMany(Insurance::class);
    }
}
