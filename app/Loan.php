<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $fillable = [
        'period_id',
        'ac_type_id',
        'loan_class_id',
        'BranchCode',
        'AcType',
        'MainCode',
        'FirmName',
        'Pro_Name',
        'ClientCode',
        'Limit',
        'Balance',
        'PenalInt',
        'IntOnInt',
        'NormalInt',
        'IntOs',
        'Class',
        'Status',
    ];

    public function acType()
    {
        return $this->belongsTo(AcType::class);
    }
}
