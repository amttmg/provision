<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('period', 'PeriodController');
    Route::post('period/upload/{id}', 'PeriodController@upload')->name('upload');
    Route::get('period/reset/{id}', 'PeriodController@reset')->name('reset');
    Route::get('provision/report/{id}', 'PeriodController@report')->name('report');
    Route::get('provision/report/export/{id}', 'PeriodController@export')->name('export');

    Route::get('insurance', 'InsuranceController@index')->name('insurance.index');
    Route::delete('insurance/{id}', 'InsuranceController@delete')->name('insurance.delete');
    Route::post('insurance/get', 'InsuranceController@getInsurances')->name('insurance.get');
    Route::patch('insurance/update/{id}', 'InsuranceController@update')->name('insurance.update');
    Route::post('insurance/store', 'InsuranceController@store')->name('insurance.store');
    Route::post('insurance/batch-import', 'InsuranceController@batchImport')->name('insurance.import');
    Route::get('insurance/get-companies', 'InsuranceController@getCompanies')->name('insurance.companies');
});
