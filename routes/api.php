<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth')->group(function () {
    Route::get('/get-loans/{id}', 'Api\LoanApiController@getLoans');
    Route::get('/get-loan-classes', 'Api\LoanApiController@getLoanClasses');
    Route::post('/update-loan-class', 'Api\LoanApiController@updateLoanClass');
});
